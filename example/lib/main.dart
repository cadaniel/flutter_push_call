import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_push_call/flutter_push_call.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  FlutterPushCall pushCall;

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    pushCall = FlutterPushCall(onTokeUpdate: onTokenUpdate);
    pushCall.getNotificationPermission();
  }

  void onTokenUpdate(String token){
    print("Native: $token");
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Text('Running on:\n'),
        ),
      ),
    );
  }
}
