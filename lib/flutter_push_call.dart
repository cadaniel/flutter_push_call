import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

class FlutterPushCall {
  final Function onTokeUpdate;
  static const MethodChannel _channel =
      const MethodChannel('flutter_push_call');

  FlutterPushCall({@required this.onTokeUpdate}) {
    _channel.setMethodCallHandler(_handler);
  }

  Future<dynamic> _handler(MethodCall call){
    if (call.method == "updateToken") {
      onTokeUpdate(call.arguments);
    }
    return null;
  }

  Future<String> getNotificationPermission() async {
    final String result = await _channel.invokeMethod('requestPermission');
    return result;
  }


}
