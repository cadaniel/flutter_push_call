import Flutter
import UIKit
import PushKit
import UserNotifications
import CallKit

public class SwiftFlutterPushCallPlugin: NSObject, FlutterPlugin, PKPushRegistryDelegate, CXProviderDelegate {
    public func providerDidReset(_ provider: CXProvider) {
        
    }
    
    static var instance: SwiftFlutterPushCallPlugin? = nil
    static var channel: FlutterMethodChannel? = nil
  public static func register(with registrar: FlutterPluginRegistrar) {
    channel = FlutterMethodChannel(name: "flutter_push_call", binaryMessenger: registrar.messenger())
    instance = SwiftFlutterPushCallPlugin()
    registrar.addMethodCallDelegate(instance!, channel: channel!)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    if (call.method == "requestPermission") {
        requestPermission(result: result)
        return
    }
  }
    
    private func requestPermission(result: @escaping FlutterResult){
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
            if (granted) {
                result("true")
            } else {
                result("false")
            }
        }
        
    }
    
    public func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.map { String(format: "%02.2hhx", $0)}.joined()
        NSLog("\n ${deviceTokenString}")
        SwiftFlutterPushCallPlugin.channel?.invokeMethod("updateToken", arguments: deviceTokenString)
    }
    
    public func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        print(pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined())
        SwiftFlutterPushCallPlugin.channel?.invokeMethod("updateToken", arguments: pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined())
       }
    
    public func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        
        let config = CXProviderConfiguration(localizedName: "My App")
        //config.iconTemplateImageData = UIImagePNGRepresentation(UIImage(named: "pizza")!)
        //config.ringtoneSound = "ringtone.caf"
        //config.includesCallsInRecents = false;
        config.supportsVideo = true;
        let provider = CXProvider(configuration: config)
        provider.setDelegate(self, queue: nil)
        let update = CXCallUpdate()
        update.remoteHandle = CXHandle(type: .generic, value: "Pete Za")
        update.hasVideo = true
        provider.reportNewIncomingCall(with: UUID(), update: update, completion: { error in })
    }
}
